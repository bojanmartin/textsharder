define([
	'forge/cipher',
	'forge/random',
	'forge/pbkdf2',
	'forge/util'
],
function(cipher, rnd, pbkdf2, util){
	var HASH_KEY_LEN = 512; // 256-bit keys for hashing should be sufficient for the next 20 years (see http://www.keylength.com/en/5/) but a number of sources also indicate that having a key length equal in size to the block size of the hashing algorithm is a good idea
	var ENCRYPTION_KEY_LEN = 128; // 128-bit keys for symmetric encryption should be sufficient for the next 20 years; see http://www.keylength.com/en/5/
	var SALT_LEN = 256; // 64-bit salts would be ok (see https://www.owasp.org/index.php/Password_Storage_Cheat_Sheet), but 256-bit salts are better as a number of sources indicate that having a salt size equal in length to the hash function's output size is a good idea
	var PREFERRED_CIPHER = 'AES-CBC'; // go with AES in CBC mode
	var PREFERRED_CIPHER_BLOCK_LENGTH = 128; // AES has a block length of 128 bits
	var NUM_PBKDF2_ITERATIONS = 50000; // FIXME is this a good number?

	// for some reason the forge stuff needs to be executed first to become useful... and it can't handle its own instance of forge
	var forge = {}; // you're welcome.
	cipher = cipher(forge);
	rnd = rnd(forge);
	pbkdf2 = pbkdf2(forge);
	util = util(forge);

	function generateKeyMaterialFromPassword(password, salt, iv){
		// forge likes to talk in bytes instead of bits so the length constants above need to be divided by 8
		var keyLen = HASH_KEY_LEN + ENCRYPTION_KEY_LEN;
		var salt = salt || rnd.getBytesSync(SALT_LEN/8);
		var iv = iv || rnd.getBytesSync(PREFERRED_CIPHER_BLOCK_LENGTH/8);
		var key = pbkdf2(password, salt, NUM_PBKDF2_ITERATIONS, (keyLen/8));
		var encryptionKey = key.slice(0, ENCRYPTION_KEY_LEN/8);
		var macKey = key.slice(ENCRYPTION_KEY_LEN/8);

		return {
			salt: salt,
			iv: iv,
			encryptionKey: encryptionKey,
			macKey: macKey
		}
	}

	function encrypt(cleartextData, encryptionKey, iv){
		var encryptor = cipher.createCipher(PREFERRED_CIPHER, encryptionKey);
		encryptor.start({iv: iv});
		encryptor.update(util.createBuffer(cleartextData));
		encryptor.finish();
		var encrypted = encryptor.output;

		return encrypted.getBytes();
	}

	function decrypt(ciphertextData, encryptionKey, iv){
		var decryptor = cipher.createDecipher(PREFERRED_CIPHER, encryptionKey);
		decryptor.start({iv: iv});
		decryptor.update(util.createBuffer(ciphertextData));
		decryptor.finish();
		var decrypted = decryptor.output;
		
		return decrypted.getBytes();
	}
	
	return {
		generateKeyMaterialFromPassword : generateKeyMaterialFromPassword,
		encrypt : encrypt,
		decrypt : decrypt
	};
});