define([
	'qrcode',
	'jquery',
	'util/util',
	'text!/html/templates/shard.htm'
],
function(qrcode, jquery, util, shardTemplate){

	var TAMPER_COLOR = '#FFC441';

	function exportShards(shards){
		_.each(shards, function(shard){
			var shardDiv = jquery(shardTemplate);
			var qrcodeDivPre = shardDiv.find('.text-sharder-shard-qr pre');
			var jsonDivPre = shardDiv.find('.text-sharder-shard-json pre');
			if(typeof shard == "object"){
				var json = JSON.stringify(shard,null,2);
				makeQRCode(qrcodeDivPre, json);
				jsonDivPre.append(json);
			}
			else{
				makeQRCode(qrcodeDivPre, shard);
				jsonDivPre.append(shard);
				jsonDivPre.addClass('base64');
			}

			var qrDiv = shardDiv.find('.text-sharder-shard-qr');
			var jsonDiv = shardDiv.find('.text-sharder-shard-json');
			var optionsDiv = shardDiv.find('.text-sharder-shard-options');
			var fo = qrDiv;

			shardDiv.find('li a').click(function(e){
				e.preventDefault();
				$(this).tab('show');

				var changed = false;
				if(shardDiv.find('.text-sharder-shard-json-title.active').length == 1){
					fi = jsonDiv;
					changed = fo == fi ? false : true;
				}
				if(shardDiv.find('.text-sharder-shard-qr-title.active').length == 1){
					fi = qrDiv;
					changed = fo == fi ? false : true;
				}
				if(shardDiv.find('.text-sharder-shard-options-title.active').length == 1){
					fi = optionsDiv;
					changed = fo == fi ? false : true;
				}

				if(changed){
					fo.fadeOut(function(){
						fi.fadeIn();
						fo = fi;
					});
				}
			});

			shardDiv.find('.text-sharder-shard-delete-btn').click(function(){
				shardDiv.css('visibility', 'hidden');
				shardDiv.find('.text-sharder-shard-json pre').remove();
			});

			shardDiv.find('.text-sharder-shard-tamper-data-btn').click(function(){
				var el = shardDiv.find('.text-sharder-shard-json pre');
				tamperElement(el, 'dat');
				makeQRCode(qrcodeDivPre, el[0].innerText, TAMPER_COLOR);
			});

			shardDiv.find('.text-sharder-shard-tamper-mac-btn').click(function(){
				var el = shardDiv.find('.text-sharder-shard-json pre');
				tamperElement(el, 'mac');
				makeQRCode(qrcodeDivPre, el[0].innerText, TAMPER_COLOR);
			});

			shardDiv.find('.text-sharder-shard-tamper-set-mac-btn').click(function(){
				var el = shardDiv.find('.text-sharder-shard-json pre');
				tamperElement(el, 'set_mac');
				makeQRCode(qrcodeDivPre, el[0].innerText, TAMPER_COLOR);
			});

			jsonDiv.hide();
			optionsDiv.hide();
			jquery('.text-sharder-shards').append(shardDiv).hide().fadeIn();
		});
	}

	function makeQRCode(qrcodeDivPre, text, colorLight){
		colorLight = colorLight || "#FFFFFF";
		var pre = qrcodeDivPre[0];
		pre.innerHTML = "";
		text = text.replace(/\s/g, "");
		var qrcode = new QRCode(pre, {
			text: text,
			width: 200,
			height: 200,
			colorDark : "#333366",
			colorLight : colorLight,
			correctLevel : QRCode.CorrectLevel.M
		});
	}

	function tamperElement(element, key){
		var el = element[0];
		var text = el.innerText;
		var obj;
		var base64Envelope = false;
		try {
			obj = JSON.parse(text);
		}
		catch(e){
			obj = JSON.parse(util.fromBase64(text, "string"));
			base64Envelope = true;
		}
		tamperObject(obj, key);
		if(base64Envelope){
			text = JSON.stringify(obj);
			text = util.toBase64(text);
		}
		else{
			text = JSON.stringify(obj,null,2);
		}
		el.innerText = text;
		element.css('background-color', TAMPER_COLOR);
	}

	function tamperObject(obj, key){
		var tamperedValues = ['y0uveBeenH4x0rd', 'h4x', '4n0TH3r1bit357h3du5T'];
		var tamperedValue = tamperedValues[Math.floor(Math.random() * (tamperedValues.length))];
		obj[key] = tamperedValue;
	}
	
	return {
		exportShards : exportShards
	};
});