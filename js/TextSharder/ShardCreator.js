define([
	'util/util',
	'ReedSolomon',
	'./ShardMACer',
	'./ShardEncryption',
	'underscore'
],
function(util, ReedSolomon, ShardMACer, ShardEncryption, _){
	var version = "0.1.0.0";

	function createShards(dataToShard, numShards, numReqdShards, maxShardSize, password, base64EncodeEnvelopes){
		// TODO error checking on parameters
		var includePasswordInEnvelope = false;
		if(typeof password == "undefined" || password == null || password.trim().length == 0){
			password = util.generateRandomPassword();
			includePasswordInEnvelope = true;
		}

		var keyMaterial = ShardEncryption.generateKeyMaterialFromPassword(password);

		var encDataToShard = ShardEncryption.encrypt(dataToShard, keyMaterial.encryptionKey, keyMaterial.iv);

		var dataLen = encDataToShard.length; // encDataToShard is likely larger than dataToShard (because of padding during encryption) so this is the right length to use for dataLen
		var parLen = Math.ceil(((numShards-numReqdShards)/numReqdShards)*dataLen);

		if(parLen == 0){
			parLen = 1; // the ReedSolomon lib doesn't agree to make 0 parity bits, so we'll give it just 1 to make it happy
		}

		var totalLen = dataLen + parLen;
		var parPadLen = ((Math.ceil(totalLen/numShards)*numShards)-totalLen); // add padding so that the totalLen divides evenly into numShards (what better place to put it than in the parity?)
		parLen += parPadLen;
		totalLen += parPadLen;
		var shardLen = totalLen/numShards;

		var fakeEnvelope = wrapInEnvelope(version, "string", numShards, numShards, parLen, "", keyMaterial);
		ShardMACer.addMAC(fakeEnvelope, keyMaterial.macKey);
		ShardMACer.addSetMAC([fakeEnvelope], keyMaterial.macKey);
		var fakeEnvelopeLen = JSON.stringify(fakeEnvelope).length;

		var shardWithBase64OverheadLen = 4 * Math.ceil(shardLen/3);
		var shardWithBase64OverheadAndEnvelopeLen = shardWithBase64OverheadLen + fakeEnvelopeLen;
		if(base64EncodeEnvelopes){
			shardWithBase64OverheadAndEnvelopeLen = 4 * Math.ceil(shardWithBase64OverheadAndEnvelopeLen/3);
		}
		if(shardWithBase64OverheadAndEnvelopeLen>maxShardSize){
			console.log('shardWithBase64OverheadAndEnvelopeLen > maxShardSize');
			throw new Error('The shards (of size ' + shardWithBase64OverheadAndEnvelopeLen + 'B) would be larger than the desired size (' + maxShardSize + 'B)');
		}

		var rs = new ReedSolomon(parLen);
		var encDataToShardWithParity = rs.encode(encDataToShard);

		var slicedData = util.slice(encDataToShardWithParity, shardLen);

		var slicesEncodedWithEnvelope = _.map(slicedData, function wrap(slice, sliceNum){
			var base64EncodedSlice = util.toBase64(slice);
			var sliceWithEnvelope = wrapInEnvelope(version, "string", (sliceNum+1), numShards, parLen, base64EncodedSlice, keyMaterial);
			if(includePasswordInEnvelope){
				sliceWithEnvelope.pwd = password;
			}
			ShardMACer.addMAC(sliceWithEnvelope, keyMaterial.macKey);
			return ShardMACer.verifyMAC(sliceWithEnvelope, keyMaterial.macKey) ? sliceWithEnvelope : null;
		});
		
		ShardMACer.addSetMAC(slicesEncodedWithEnvelope, keyMaterial.macKey);
		var shards = ShardMACer.verifySetMAC(slicesEncodedWithEnvelope, keyMaterial.macKey) ? slicesEncodedWithEnvelope : null;
		if(shards && base64EncodeEnvelopes){
			return _.map(shards, function(shard){
				return util.toBase64(JSON.stringify(shard));
			});
		}
		else{
			return shards;
		}
	}

	function wrapInEnvelope(version, type, shardIndex, numShards, parLen, data, keyMaterial){
		var saltBase64 = util.toBase64(keyMaterial.salt);
		var ivBase64 = util.toBase64(keyMaterial.iv);

		var metadata = {
			typ: type,
			shd_ord: shardIndex, // ordinal
			set_car: numShards, // cardinality
			par_len: parLen
		};
		metadata = JSON.stringify(metadata);
		metadata = ShardEncryption.encrypt(metadata, keyMaterial.encryptionKey, keyMaterial.iv);
		metadata = util.toBase64(metadata);

		return {
			ver: version,
			meta: metadata,
			dat: data,
			slt: saltBase64, // salt
			iv: ivBase64
		};
	}
	
	return {
		createShards : createShards
	};
});