define([
	'forge/hmac',
	'util/util',
	'underscore'
],
function(hmac, util, _){

	hmac = hmac();

	function addMAC(shard, key){
		shard.mac = calculateShardMAC(shard, key);
	}

	function verifyMAC(shard, key){
		return shard.mac == calculateShardMAC(shard, key);
	}

	function calculateStringMAC(str, key){
		var md = hmac.create();
		md.start('sha256', key);
		md.update(str);
		return util.toBase64(md.digest().bytes());
	}

	function calculateShardMAC(shard, key){
		var cat = concatenateDesiredElementsForMAC(shard, true);
		return calculateStringMAC(cat, key);
	}

	function calculateShardSetMAC(shards, key){
		var sortedShards = _.sortBy(shards, function(shard){
			return shard.shd_ord;
		});
		var cat = "[";
		_.each(sortedShards, function(shard, idx){
			cat += concatenateDesiredElementsForMAC(shard, false);
			if(idx<sortedShards.length-1){
				cat += ',';
			}
		});
		cat += "]";
		return calculateStringMAC(cat, key);
	}

	function concatenateDesiredElementsForMAC(shard, excludeMac){
		var keys = Object.keys(shard);
		if(excludeMac){
			keys = _.reject(keys, function(key){
				return key == 'mac';
			});
		}
		keys = _.reject(keys, function(key){
			return key == 'set_mac';
		});
		keys.sort();
		var cat = "{";
		_.each(keys, function(key, idx){
			cat += '"' + key + '":"' + shard[key] + '"';
			if(idx<keys.length-1){
				cat += ',';
			}
		});
		cat += "}";
		return cat;
	}

	function addSetMAC(shards, key){
		var mac = calculateShardSetMAC(shards, key);
		_.each(shards, function(shard){
			shard.set_mac = mac;
		});
	}

	function verifySetMAC(shards, key){
		var setMac = calculateShardSetMAC(shards, key);
		var allGood = true;
		_.each(shards, function(shard){
			allGood = allGood && shard.set_mac;
		});
		return allGood;
	}
	
	return {
		addMAC : addMAC,
		verifyMAC : verifyMAC,
		addSetMAC : addSetMAC,
		verifySetMAC : verifySetMAC
	};
});