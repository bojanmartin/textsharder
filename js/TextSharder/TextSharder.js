define([
	'./ShardCreator',
	'./ShardExporter',
	'./ShardCollector',
	'./TextReconstructor',
	'jquery',
	'underscore'
],
function(ShardCreator, ShardExporter, ShardCollector, TextReconstructor, jquery, _){

	var textToShard = "5Kf6Pfg2wzPec5e1fjSTWTUDBiqTiS1VbJGtasd4ZoBVLyMAmsT"; // a BTC private key, etc
	var numShards = 10;
	var numReqdShards = 8;
	var maxSize = 1024;
	var password = 'passw0rd';
	var base64EncodeEnvelopes = false;

	var reconstructionSuccess;
	var reconstructionAttempt = 0;
	var curReconstructionAttempt = 0;

	function shard(){
		jquery(document).ready(function(){
			jquery('.text-sharder-shards').empty();
			setVarsFromInputs();
			var shards = ShardCreator.createShards(textToShard, numShards, numReqdShards, maxSize, password, base64EncodeEnvelopes);
			ShardExporter.exportShards(shards);
		});
	}

	function setVarsFromInputs(){
		textToShard = getValOrPlaceholder('.desired-text');
		numShards = Number.parseInt(getValOrPlaceholder('.desired-num-total-shards'));
		numReqdShards = Number.parseInt(getValOrPlaceholder('.desired-num-min-shards'));
		maxSize = Number.parseInt(getValOrPlaceholder('.desired-max-shard-size'));
		password = getValOrPlaceholder('.desired-password');
	}

	function getValOrPlaceholder(selector){
		var el = jquery(selector);
		var val = el.val().trim();
		return val.length > 0 ? val : el.attr('placeholder');
	}

	function reconstruct(){
		if(curReconstructionAttempt != reconstructionAttempt){
			curReconstructionAttempt = reconstructionAttempt;
			jquery(document).ready(function(){
				var collectedShards = ShardCollector.collectShards();
				jquery('.text-sharder-reconstructed-text').empty();
				try{
					var reconstructedText = TextReconstructor.reconstructText(collectedShards, password);
					jquery('.text-sharder-reconstructed-text').append('<span class="glyphicon glyphicon-th" aria-hidden="true"></span> ' + reconstructedText);
					reconstructionSuccess = true;
				}
				catch(e){
					jquery('.text-sharder-reconstrucion-error').append('<span class="glyphicon glyphicon-warning-sign" aria-hidden="true"></span> ' + e);
					reconstructionSuccess = false;
				}
			});
		}
	}

	jquery(document).ready(function(){
		jquery('.text-sharder-shard-btn').click(function(){
			jquery('.shards .alert').fadeOut(function(){
				jquery('.shards .alert-info').fadeIn(function(){
					shard();
					jquery('.shards .alert-info').fadeOut(function(){
						jquery('.shards .alert-success, .shards .alert-danger').fadeIn();
					});
				});
			});
		});

		jquery('.text-sharder-reconstruct-btn').click(function(){
			reconstructionAttempt++;
			jquery('.reconstruction .alert-success, .reconstruction .alert-danger').fadeOut(function(){
				jquery('.reconstruction .alert-info').fadeIn(function(){
					reconstruct(reconstructionAttempt);
					jquery('.reconstruction .alert-info').fadeOut(function(){
						if(reconstructionSuccess){
							jquery('.reconstruction .alert-success').fadeIn();
						}
						else{
							jquery('.reconstruction .alert-danger').fadeIn();
						}
					});
				});
			});
		});

		jquery('.alert').hide();
	});

	shard();
});