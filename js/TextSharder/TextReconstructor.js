define([
	'util/util',
	'ReedSolomon',
	'./ShardMACer',
	'./ShardEncryption',
	'underscore'
],
function(util, ReedSolomon, ShardMACer, ShardEncryption, _){
	var ERASURE = -1;

	var version = "0.1.0.0"; // TODO verify version is compatible

	var keyMaterialCache = {}; // this shouldn't be necesssary but is a safe way of dealing with the shards if there are for some reason multiple salts and/or IVs (this shouldn't happen); things will also obviously go much faster if the key material is not recomputed for every shard

	function reconstructText(shards, password){
		var base64DecodedShards = _.map(shards, function(shard){
			if(typeof shard == "object"){
				return shard;
			}
			else{
				try{
					return JSON.parse(util.fromBase64(shard, "string"));
				}
				catch(e){
					return null;
				}
			}
		});

		var filteredShards = filterShardsBySetMac(base64DecodedShards);

		if(typeof password == "undefined" || password == null || password.trim().length == 0){
			var aShard = filteredShards[0];
			if(typeof aShard.pwd == "undefined" || aShard.pwd == null || aShard.pwd.trim().length == 0){
				throw "the shards do not contain the password; please pass in a password";
			}
			else{
				password = aShard.pwd;
			}
		}

		// TODO if all shards are present, verify the set MAC

		// first thing to do is verify the MAC
		var goodShards = _.filter(filteredShards, function(shard){
			var keyMaterial = getKeyMaterial(password, shard.slt, shard.iv);
			return ShardMACer.verifyMAC(shard, keyMaterial.macKey);
		});

		_.each(goodShards, function(shard){
			decryptMetadata(shard, password);
		});

		var sortedShards = _.sortBy(goodShards, function(shard){
			return shard.meta.shd_ord;
		});

		var parLen = null;
		var previousShardOrd = 0;
		var arrOfArrOfEncDataWithParity = _.map(sortedShards, function(shard, idx){
			var missingShardsBefore = shard.meta.shd_ord - previousShardOrd - 1;
			var missingShardsAfter = 0;
			previousShardOrd = shard.meta.shd_ord;
			var uint8arr = util.fromBase64(shard.dat);
			if(idx == sortedShards.length-1){
				missingShardsAfter = shard.meta.set_car - shard.meta.shd_ord;
			}
			var shardDataLen = uint8arr.length + ((missingShardsBefore+missingShardsAfter)*uint8arr.length);
			var arr = Array(shardDataLen);
			var i = 0;
			for(i; i<missingShardsBefore*uint8arr.length; i++){
				arr[i] = ERASURE;
			}
			if(parLen == null){
				parLen = shard.meta.par_len;
			}
			if(parLen != shard.meta.par_len){
				var offset = i;
				for(i; i<uint8arr.length+offset; i++){
					arr[i] = ERASURE;
				}
			}
			else{
				var view = new DataView(uint8arr.buffer);
				var offset = i;
				for(var j = 0; i<uint8arr.length+offset; j++){
					arr[i] = view.getUint8(j);
					i++;
				}
			}
			for(i; i<arr.length; i++){
				arr[i] = ERASURE;
			}
			return arr;
		});

		var arrOfEncDataWithParity = [].concat.apply([], arrOfArrOfEncDataWithParity);
		
		var rs = new ReedSolomon(parLen);
		var encData = rs.decode(arrOfEncDataWithParity);

		var cacheKey = _.filter(Object.keys(keyMaterialCache), function(objKey){
			var aShard = sortedShards[0];
			return objKey == aShard.slt+aShard.iv;
		})[0];
		var keyMaterial = keyMaterialCache[cacheKey];
		var decryptedData = ShardEncryption.decrypt(encData, keyMaterial.encryptionKey, keyMaterial.iv);

		return decryptedData;
	}

	function getKeyMaterial(password, salt, iv){
		var cacheKey = salt+iv;
		if(typeof keyMaterialCache[cacheKey] == "undefined"){
			keyMaterialCache[cacheKey] = ShardEncryption.generateKeyMaterialFromPassword(password, util.fromBase64(salt, 'string'), util.fromBase64(iv, 'string'));
		}
		return keyMaterialCache[cacheKey];
	}

	function filterShardsBySetMac(shards){
		var groups = _.groupBy(shards, function(shard){
			return shard.set_mac;
		});
		var sorted = _.sortBy(groups, function(group, set_mac){
			return group.length;
		});
		return _.last(sorted);
	}

	function decryptMetadata(shard, password){
		var keyMaterial = getKeyMaterial(password, shard.slt, shard.iv);
		var metadata = util.fromBase64(shard.meta, 'string');
		metadata = ShardEncryption.decrypt(metadata, keyMaterial.encryptionKey, keyMaterial.iv);
		metadata = JSON.parse(metadata);
		shard.meta = metadata;
	}
	
	return {
		reconstructText : reconstructText
	};
});