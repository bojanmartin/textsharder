define([
	'jquery',
	'underscore'
],
function(jquery, _){
	function collectShards(){
		var rawShards = jquery('.text-sharder-shard-json pre');
		var shards = _.map(rawShards, function(rawShard){
			try{
				return JSON.parse(rawShard.innerText);
			}
			catch(e){
				return rawShard.innerText;
			}
		});
		return shards;
	}
	
	return {
		collectShards : collectShards
	};
});