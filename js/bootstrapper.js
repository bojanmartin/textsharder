require.config({
    paths: {
        'jquery': 'lib/jquery-2.1.1.min',
        'underscore': 'lib/underscore-1.7.0.min',
        'bootstrap': 'lib/bootstrap-3.2.0.min',
        'class': 'lib/class',
        'ReedSolomon': "lib/erc-js/ReedSolomon",
        'forge': "lib/forge",
        'qrcode': "lib/qrcodejs/qrcode.min",
        'text': 'lib/text'
    },
    shim: {
        'bootstrap': {
            deps: ['jquery']
        },
        'class': {
            exports: 'Class'
        },
        'ReedSolomon': {
            deps: ['class'],
            exports: 'ReedSolomon'
        },
        'qrcode': {
            exports: 'QRCode'
        }
    }
});

require(['main'], function(){
	// main has now executed
});