define([
	'jquery',
	'bootstrap'
],
function(jquery, bootstrap){
	var head = jquery('head');
	head.append('<meta name="viewport" content="width=device-width, initial-scale=1">');
	head.append('<link rel="stylesheet" href="css/lib/bootstrap-3.2.0.min.css">');
	head.append('<link rel="stylesheet" href="css/lib/bootstrap-theme-3.2.0.min.css">');
	head.append('<link rel="stylesheet" href="css/TextSharder.css">');
});