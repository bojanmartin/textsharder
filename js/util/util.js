define([
	'forge/util',
	'forge/random'
],
function(util, rnd){

	util = util();
	rnd = rnd();

	function toBase64(arrayOrString){
		var ui8arr;
		if(Array.isArray(arrayOrString)){
			ui8arr = new Uint8Array(arrayOrString.length);
			ui8arr.set(arrayOrString);
		}
		else if(typeof arrayOrString == 'string'){
			ui8arr = util.binary.raw.decode(arrayOrString);
		}
		
		return util.binary.base64.encode(ui8arr);
	}

	function fromBase64(base64, arrayOrString){
		var arrayOrString = arrayOrString || 'array';
		var ui8arr = util.binary.base64.decode(base64);
		if(arrayOrString == 'string'){
			return util.binary.raw.encode(ui8arr);
		}
		else{
			return ui8arr;
		}
	}

	function slice(arr, sliceLen){
		var idx = 0;
		var arrOfArr = [];
		while(true){
			if(idx+sliceLen<arr.length){
				arrOfArr.push(arr.slice(idx, sliceLen+idx));
				idx += sliceLen;
			}
			else{
				arrOfArr.push(arr.slice(idx));
				break;
			}
		}
		return arrOfArr;
	}

	function generateRandomPassword(length){
		var length = length || 16;
		var byteLen = Math.ceil(length / 4) * 3; // the maximum we'll need to produce a base-64-encoded string that has the desired length
		var pw = rnd.getBytesSync(byteLen);
		return toBase64(pw).substring(0, length);
	}
	
	return {
		toBase64 : toBase64,
		fromBase64 : fromBase64,
		slice : slice,
		generateRandomPassword : generateRandomPassword
	};
});